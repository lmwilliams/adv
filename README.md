# ADV CI Workshop Catch Up


to recreate this workshop in your GitLab instance:

1. Download the Project extract (.gz file in the repo)
1. Download the PDF Guide (.pdf in the repo above)
1. Go to your instance of GitLab
1. Select New Project
1. Select Import tab
1. Choose a Name
1. Select the downloaded file
1. Select Import


once it is imported follow the steps in the PDF Guide in the Repo

